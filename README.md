# PaymentBox

[Link to our repository](https://repository-dev.parkadmin.com/php)

## Updating the code

Make sure you push the changes to master.

The version tag should be in the format of v2.3.4 incrementing each time.

After pushing the code, also push the tag. This might need to be done manually as gitKraken doesn't push tags automatically.

## Updating the repository

To update the repository where this code sits:
1) Go to DEV-2K8
2) Navigate to D:\websites\Repository\php
3) Run the update.ps1 file in powershell, it will update the code and rebuild the repository.


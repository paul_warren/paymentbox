<?php

namespace Tomahawk\PaymentBox;

use Tomahawk\PaymentBox\Exceptions\InvalidException;

class PaymentRequest
{
    const REQUIRED = 1;
    const OPTIONAL = 2;

    /** Bitwise settings */
    const VALIDATION_AVS = 1;
    const VALIDATION_CVV = 2;
    const VALIDATION_ALL = self::VALIDATION_AVS | self::VALIDATION_CVV;

    protected $request;

    public function __construct($requestArray)
    {
        if ( ! is_array($requestArray)) {
            throw new InvalidException('The request supplied is not a valid array.');
        }

        $validParameters = [
            'amount'   => new PaymentRequestParameter(true),
            'refNum'   => new PaymentRequestParameter(true),
            'clientIP' => new PaymentRequestParameter(true),
            'clientID' => new PaymentRequestParameter(true),
            'cardname' => new PaymentRequestParameter(true),
            'card'     => new PaymentRequestParameter(true),
            'expMM'    => new PaymentRequestParameter(true),
            'expYY'    => new PaymentRequestParameter(true),

            'cvv2'       => new PaymentRequestParameter(false),
            'zipCode'    => new PaymentRequestParameter(false, true, 10),
            'avsdetails' => new PaymentRequestParameter(false, false, false),
        ];

        foreach ($validParameters as $key => $parameter) {

            if ($parameter->required && ! isset($requestArray[$key])) {
                throw new InvalidException('The required parameter ' . $key . ' was not set.');
            }

            if (isset($requestArray[$key])) {
                $value = $requestArray[$key];
                if ($parameter->trim) {
                    $value = trim($value);
                }
                if ($parameter->length !== false) {
                    $value = substr($value, 0, $parameter->length);
                }
                $this->request[$key] = $value;
            }
        }
    }

    public function __get($name)
    {
        return isset($this->request[$name]) ? $this->request[$name] : null;
    }

    public function dump()
    {
        $array = $this->request;
        unset($array['card']);

        return $array;
    }
}
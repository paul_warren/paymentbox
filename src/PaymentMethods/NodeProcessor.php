<?php

namespace Tomahawk\PaymentBox\PaymentMethods;

use Tomahawk\PaymentBox\PaymentMethod;
use Tomahawk\PaymentBox\PaymentRequest;
use Tomahawk\PaymentBox\PaymentResponse;

class NodeProcessor extends PaymentMethod
{
    protected $gateway;
    protected $parameters;
    protected $validation;
    protected $timeout;
    protected $timeout_initialization = 30; // The number of seconds to time out if the payment still hasn't at least been created.
//    protected $dynamicDescriptor = null;
    protected $test = false;
    protected $useragent = 'PHP UA - PaymentBoxAPI - 1.0.0';

    /**
     * NodeProcessor constructor.
     *
     * @param $gateway
     * @param $parameters
     * @param bool $useAVS
     * @param int $timeout
     *
     * @throws \Exception
     */
    public function __construct($gateway, $parameters, $validation = 0, $timeout = 90)
    {
        $validGateways = [
            'Moneris'         => 1,
            'ChasePaymentech' => 1
        ];

        if ( ! isset($validGateways[$gateway])) {
            throw new \Exception('Invalid Gateway option: ' . $gateway, '1138');
        }
        $this->gateway    = $gateway;
        $this->parameters = $parameters;
        $this->validation = $validation;
        $this->timeout    = $timeout;
    }

//    public function setDynamicDescriptor($text)
//    {
//        $this->dynamicDescriptor = $text;
//    }

    public function setTestGateway($test)
    {
        $this->test = $test;
    }

    public function attempt($total = 0.00, PaymentRequest $request)
    {
//        if ($this->test) {
//            $host  = "https://orbitalvar1.paymentech.net";
//            $host2 = "https://orbitalvar2.paymentech.net"; // failover
//            // TODO for now, i'm likely not going to code for retries. maybe in the future if necessary, but not now.
//        } else {
//            $host  = "https://orbital1.paymentech.net";
//            $host2 = "https://orbital2.paymentech.net"; // failover
//        }
//        $path    = "/authorize";

        /************************* Transactional Variables ****************************/

        $postArray = [];

        if ($this->validation & PaymentRequest::VALIDATION_AVS) {
            $postArray = $request->avsdetails;
        }

        $postArray = array_merge(
            $postArray,
            $this->parameters,
            [
                'clientID' => $request->clientID,
                'clientIP' => $request->clientIP,
                'refNum'   => $request->refNum,
                'amount'   => $total,
                'card'     => $request->card,
                'expMM'    => $request->expMM,
                'expYY'    => $request->expYY,
                'end'      => 1,
            ]
        );
        if ($this->validation & PaymentRequest::VALIDATION_AVS) {
            $postArray['cvv2'] = $request->cvv2;
        }

        try {

            if ($this->test) {
                $nodeHost1 = 'http://192.168.10.100:3000/steelArrow/queuePayment/' . $this->gateway;
                $nodeHost2 = 'http://192.168.10.100:3000/steelArrow/getPayment/' . $request->refNum;
            } else {
                $nodeHost1 = 'http://192.168.2.100:3000/steelArrow/queuePayment/' . $this->gateway;
                $nodeHost2 = 'http://192.168.2.100:3000/steelArrow/getPayment/' . $request->refNum;
            }

            // $response =
            $this->nodeProcessor($nodeHost1, $postArray);
            // give Node some time to process the request. The whole request shouldn't take longer than $timeout.
            sleep(2);

            $timer = microtime(true);


            do {
                $tryAgain = false;

                $response = $this->nodeProcessor($nodeHost2);
                if (preg_match('/Queued|(No Reference ID Found)/i', $response)) {

                    if (stripos('No Reference ID Found', $response) !== false && microtime(true) - $timer > $this->timeout_initialization) {
                        $tryAgain = false;
                        // Abort because it's been $timeout_initialization seconds since the payment was attempted and there is still no payment record created.
                    } else {
                        $tryAgain = true;
                        sleep(5);
                    }
                }
            } while ($tryAgain && microtime(true) - $timer < $this->timeout);


            if ($response) {
                $exploded = explode(":::", $response);
                if (isset($exploded[0]) && stripos($exploded[0], 'approved') !== false) {

//                $lines[] = "========== TRANSACTION RECORD ==========";
//                $lines[] = "TYPE         :  Purchase";
//                $lines[] = "";
//                $lines[] = "ACCT         :  ".(isset($cardTypes[$mpgResponse->getCardType()])? $cardTypes[$mpgResponse->getCardType()] : ''). " ".$mpgResponse->getTransAmount()." ".$this->currency;
//                $lines[] = "";
//                $lines[] = "CARD NUMBER  :  ".$this->maskCreditCard($request->card);
//                $lines[] = "REF. NUMBER  :  ".$mpgResponse->getReferenceNum();
//                $lines[] = "CARD HOLDER  :  ".$request->cardname;
//                $lines[] = "DATE/TIME    :  ".$mpgResponse->getTransDate()." ".$mpgResponse->getTransTime();
//                $lines[] = "";
//                $lines[] = "MESSAGE      :  ".$mpgResponse->getMessage();
//                $lines[] = "";
//                $lines[] = "========== ADDITIONAL DETAILS ==========";
//                // $lines[] = "COMPLETE     :  ".$mpgResponse->getComplete();
//                $lines[] = "TXN. NUMBER  :  ".$mpgResponse->getTxnNumber();
//                $lines[] = "RECEIPT ID   :  ".$mpgResponse->getReceiptId();
//                $lines[] = "TXN. TYPE    :  ".$mpgResponse->getTransType();
//                $lines[] = "RESPONSE CODE:  ".$mpgResponse->getResponseCode();
//                $lines[] = "ISO          :  ".$mpgResponse->getISO();
//                $lines[] = "AUTH CODE    :  ".$mpgResponse->getAuthCode();
//                $lines[] = "TICKET       :  ".$mpgResponse->getTicket();
//                $lines[] = "STATUS CODE  :  ".$mpgResponse->getStatusCode();
//                $lines[] = "STATUS MSG   :  ".$mpgResponse->getStatusMessage();
//
//                // $mpgResponse->getComplete() &&
//                if (((int) $mpgResponse->getResponseCode()) < 50) {
                    return new PaymentResponse(PaymentResponse::SUCCESS,
                        $response
                    );
                } else {
                    return new PaymentResponse(PaymentResponse::DECLINED,
                        $response
                    );
                }
            } else {
                return new PaymentResponse(PaymentResponse::ERROR_NO_RESPONSE,
                    "ERROR:::\nResponse was not received from gateway.\n"
                );
            }
        } catch (\Exception $e) {
            return new PaymentResponse(PaymentResponse::ERROR_EXCEPTION,
                "FATAL ERROR:::\n" . $e->getMessage()
            );
        }
    }

    private function nodeProcessor($url, $postdata = false, $timeout = 90)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        if ($postdata !== false) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, urldecode(http_build_query($postdata)));
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->useragent);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

        $response = curl_exec($ch);

        if (false === $response) {
            throw new \Exception(curl_error($ch), curl_errno($ch));
        }

        curl_close($ch);

        return $response;
    }
}
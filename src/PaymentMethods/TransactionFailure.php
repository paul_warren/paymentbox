<?php

namespace Tomahawk\PaymentBox\PaymentMethods;

use Tomahawk\PaymentBox\PaymentMethod;
use Tomahawk\PaymentBox\PaymentRequest;
use Tomahawk\PaymentBox\PaymentResponse;

class TransactionFailure extends PaymentMethod
{
    protected $action = self::PROCESS;

    public function attempt($total = 0.00, PaymentRequest $request)
    {
        sleep(2);

        return new PaymentResponse(PaymentResponse::FAILED,
            "ERROR:::Gateway communication failure."
        );
    }
}
<?php

namespace Tomahawk\PaymentBox\PaymentMethods;

use Tomahawk\PaymentBox\PaymentMethod;

class Zero extends PaymentMethod
{
    protected $action = self::PROCESS;

    /*
     *
            $paymentTransaction = TransactionLog::getZeroTransaction($orderAttempt->uniqueID);
            $this->finalizeOrder($paymentTransaction, $orderAttempt, $reservations, $request);

            return redirect('receipt/'.$orderAttempt->uniqueID);
     */
}
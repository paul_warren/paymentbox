<?php

namespace Tomahawk\PaymentBox\PaymentMethods;

use Tomahawk\PaymentBox\PaymentMethod;

class Cash extends PaymentMethod
{
    protected $action = self::DEFERRED;
}
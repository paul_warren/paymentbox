<?php

namespace Tomahawk\PaymentBox\PaymentMethods;

use Tomahawk\PaymentBox\PaymentMethod;
use Tomahawk\PaymentBox\PaymentRequest;
use Tomahawk\PaymentBox\PaymentResponse;

class ChasePaymentech extends PaymentMethod
{
    protected $userid;
    protected $currency;
    protected $dynamicDescriptor = null;
    protected $test = false;
    protected $useragent = 'PHP UA - ChaseAPI - 1.0.0';

    public function __construct($paymentUserID, $currency, $useAVS = true)
    {
        $this->userid   = $paymentUserID;
        $this->currency = strtoupper(substr($currency, 0, 3));
    }

    public function setDynamicDescriptor($text)
    {
        $this->dynamicDescriptor = $text;
    }

    public function setTestGateway($test)
    {
        $this->test = $test;
    }

    public function attempt($total = 0.00, PaymentRequest $request)
    {
//        if ($this->test) {
//            $host  = "https://orbitalvar1.paymentech.net";
//            $host2 = "https://orbitalvar2.paymentech.net"; // failover
//            // TODO for now, i'm likely not going to code for retries. maybe in the future if necessary, but not now.
//        } else {
//            $host  = "https://orbital1.paymentech.net";
//            $host2 = "https://orbital2.paymentech.net"; // failover
//        }
        $timeout = 90;
//        $path    = "/authorize";

        /************************* Transactional Variables ****************************/

        $postArray   = array_merge(
            $request->avsdetails,
            [
                'usdCad'   => $this->currency,
                'merchID'  => $this->userid,
                'clientID' => $request->clientID,
                'clientIP' => $request->clientIP,
                'refNum'   => $request->refNum,
                'amount'   => $total,
                'card'     => $request->card,
                'expMM'    => $request->expMM,
                'expYY'    => $request->expYY,
                'cvv2'     => $request->cvv2,
                'end'      => 1,
            ]
        );

        try {

            if ($this->test) {
                $nodeHost1 = 'http://192.168.10.100:3000/steelArrow/queuePayment/ChasePaymenTech';
                $nodeHost2 = 'http://192.168.10.100:3000/steelArrow/getPayment/'.$request->refNum;
            } else {
                $nodeHost1 = 'http://192.168.2.100:3000/steelArrow/queuePayment/ChasePaymenTech';
                $nodeHost2 = 'http://192.168.2.100:3000/steelArrow/getPayment/'.$request->refNum;
            }

            // $response =
            $this->nodeProcessor($nodeHost1, $postArray);
            sleep(2);

            $timer = microtime(true);

            do {
                $response = $this->nodeProcessor($nodeHost2);
                if (stripos($response, 'queued') !== false) {
                    sleep(5);
                }
            } while (stripos($response,'queued') !== false && microtime(true) - $timer < $timeout);


            if ($response) {
                $exploded = explode(":::",$response);
                if (isset($exploded[0]) && stripos($exploded[0],'approved') !== false) {

//                $lines[] = "========== TRANSACTION RECORD ==========";
//                $lines[] = "TYPE         :  Purchase";
//                $lines[] = "";
//                $lines[] = "ACCT         :  ".(isset($cardTypes[$mpgResponse->getCardType()])? $cardTypes[$mpgResponse->getCardType()] : ''). " ".$mpgResponse->getTransAmount()." ".$this->currency;
//                $lines[] = "";
//                $lines[] = "CARD NUMBER  :  ".$this->maskCreditCard($request->card);
//                $lines[] = "REF. NUMBER  :  ".$mpgResponse->getReferenceNum();
//                $lines[] = "CARD HOLDER  :  ".$request->cardname;
//                $lines[] = "DATE/TIME    :  ".$mpgResponse->getTransDate()." ".$mpgResponse->getTransTime();
//                $lines[] = "";
//                $lines[] = "MESSAGE      :  ".$mpgResponse->getMessage();
//                $lines[] = "";
//                $lines[] = "========== ADDITIONAL DETAILS ==========";
//                // $lines[] = "COMPLETE     :  ".$mpgResponse->getComplete();
//                $lines[] = "TXN. NUMBER  :  ".$mpgResponse->getTxnNumber();
//                $lines[] = "RECEIPT ID   :  ".$mpgResponse->getReceiptId();
//                $lines[] = "TXN. TYPE    :  ".$mpgResponse->getTransType();
//                $lines[] = "RESPONSE CODE:  ".$mpgResponse->getResponseCode();
//                $lines[] = "ISO          :  ".$mpgResponse->getISO();
//                $lines[] = "AUTH CODE    :  ".$mpgResponse->getAuthCode();
//                $lines[] = "TICKET       :  ".$mpgResponse->getTicket();
//                $lines[] = "STATUS CODE  :  ".$mpgResponse->getStatusCode();
//                $lines[] = "STATUS MSG   :  ".$mpgResponse->getStatusMessage();
//
//                // $mpgResponse->getComplete() &&
//                if (((int) $mpgResponse->getResponseCode()) < 50) {
                    return new PaymentResponse(PaymentResponse::SUCCESS,
                        $response
                    );
                } else {
                    return new PaymentResponse(PaymentResponse::DECLINED,
                        $response
                    );
                }
            } else {
                return new PaymentResponse(PaymentResponse::ERROR_NO_RESPONSE,
                    "ERROR:::\nResponse was not received from gateway.\n"
                );
            }
        } catch (\Exception $e) {
            return new PaymentResponse(PaymentResponse::ERROR_EXCEPTION,
                "FATAL ERROR:::\n" . $e->getMessage()
            );
        }
    }

    private function nodeProcessor($url, $postdata = false, $timeout = 90)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        if ($postdata !== false) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, urldecode(http_build_query($postdata)));
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->useragent);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

        $response = curl_exec($ch);

        if (FALSE === $response)
            throw new \Exception(curl_error($ch), curl_errno($ch));

        curl_close($ch);

        return $response;
    }
}
<?php

namespace Tomahawk\PaymentBox\PaymentMethods;

use Tomahawk\PaymentBox\PaymentMethod;
use Tomahawk\PaymentBox\PaymentRequest;
use Tomahawk\PaymentBox\PaymentResponse;
use Tomahawk\PaymentBox\ThirdParty\Moneris\mpgHttpsPost;
use Tomahawk\PaymentBox\ThirdParty\Moneris\mpgRequest;
use Tomahawk\PaymentBox\ThirdParty\Moneris\mpgResponse;
use Tomahawk\PaymentBox\ThirdParty\Moneris\mpgTransaction;

class Moneris extends PaymentMethod
{
    protected $userid;
    protected $password;
    protected $currency;
    protected $dynamicDescriptor = null;
    protected $test = false;

    public function __construct($paymentUserID, $paymentPassword, $currency, $useAVS = true)
    {
        $this->userid   = $paymentUserID;
        $this->password = $paymentPassword;
        $this->currency = strtoupper(substr($currency, 0, 2));
    }

    public function setDynamicDescriptor($text)
    {
        $this->dynamicDescriptor = $text;
    }

    public function setTestGateway($test)
    {
        $this->test = $test;
    }

    public function attempt($total = 0.00, PaymentRequest $request)
    {

        /************************* Transactional Variables ****************************/

        $type         = 'purchase';
        $cust_id      = $request->clientID;
        $order_id     = $request->refNum;
        $amount       = $total;
        $pan          = $request->card;
        $expiry_date  = $request->expYY . $request->expMM;
        $crypt        = '7';
        $status_check = 'false';

        // Optional - Set for Multi-Currency only
        // $amount must be 0.00 when using multi-currency
        $mcp_amount        = '500'; //penny value amount 1.25 = 125
        $mcp_currency_code = '840'; //ISO-4217 country currency number

        /*********************** Transactional Associative Array **********************/

        $txnArray = [
            'type'       => $type,
            'order_id'   => $order_id,
            'cust_id'    => $cust_id,
            'amount'     => $amount,
            'pan'        => $pan,
            'expdate'    => $expiry_date,
            'crypt_type' => $crypt,
//            'dynamic_descriptor' => $dynamic_descriptor
            //,'wallet_indicator' => '' //Refer to documentation for details
            //,'mcp_amount' => $mcp_amount,
            //'mcp_currency_code' => $mcp_currency_code
        ];
        if ($this->dynamicDescriptor) {
            $txnArray['dynamic_descriptor'] = $this->dynamicDescriptor;
        }

        $mpgTxn = new mpgTransaction($txnArray);

        /****************************** Request Object *******************************/

        $mpgRequest = new mpgRequest($mpgTxn);
        $mpgRequest->setProcCountryCode($this->currency); //"US" for sending transaction to US environment
        $mpgRequest->setTestMode($this->test); //false or comment out this line for production transactions

        /***************************** HTTPS Post Object *****************************/

        /* Status Check Example
        $mpgHttpPost  =new mpgHttpsPostStatus($store_id,$api_token,$status_check,$mpgRequest);
        */

        $mpgHttpPost = new mpgHttpsPost($this->userid, $this->password, $mpgRequest);

        /******************************* Response ************************************/

        $cardTypes = [
            'M' => 'MasterCard',
            'V' => 'Visa',
            'AX' => 'American Express',
            'NO' => 'Novus/Discover',
            'DS' => 'Discover',
            'C' => 'JCB',
            'C1' => 'JCB',
            'SE' => 'Sears',
            'CQ' => 'ACH',
            'P' => 'Pin Debit',
            'D' => 'Debit',
        ];

        try {
            $mpgResponse = $mpgHttpPost->getMpgResponse();

            $lines = [];
            if ($mpgResponse !== null && $this->isComplete($mpgResponse->getComplete())) {
                $lines[] = "========== TRANSACTION RECORD ==========";
                $lines[] = "TYPE         :  Purchase";
                $lines[] = "";
                $lines[] = "ACCT         :  ".(isset($cardTypes[$mpgResponse->getCardType()])? $cardTypes[$mpgResponse->getCardType()] : ''). " ".$mpgResponse->getTransAmount()." ".$this->currency;
                $lines[] = "";
                $lines[] = "CARD NUMBER  :  ".$this->maskCreditCard($request->card);
                $lines[] = "REF. NUMBER  :  ".$mpgResponse->getReferenceNum();
                $lines[] = "CARD HOLDER  :  ".$request->cardname;
                $lines[] = "DATE/TIME    :  ".$mpgResponse->getTransDate()." ".$mpgResponse->getTransTime();
                $lines[] = "";
                $lines[] = "MESSAGE      :  ".$mpgResponse->getMessage();
                $lines[] = "";
                $lines[] = "========== ADDITIONAL DETAILS ==========";
                // $lines[] = "COMPLETE     :  ".$mpgResponse->getComplete();
                $lines[] = "TXN. NUMBER  :  ".$mpgResponse->getTxnNumber();
                $lines[] = "RECEIPT ID   :  ".$mpgResponse->getReceiptId();
                $lines[] = "TXN. TYPE    :  ".$mpgResponse->getTransType();
                $lines[] = "RESPONSE CODE:  ".$mpgResponse->getResponseCode();
                $lines[] = "ISO          :  ".$mpgResponse->getISO();
                $lines[] = "AUTH CODE    :  ".$mpgResponse->getAuthCode();
                $lines[] = "TICKET       :  ".$mpgResponse->getTicket();
                $lines[] = "STATUS CODE  :  ".$mpgResponse->getStatusCode();
                $lines[] = "STATUS MSG   :  ".$mpgResponse->getStatusMessage();

                // $mpgResponse->getComplete() &&
                if (((int) $mpgResponse->getResponseCode()) < 50) {
                    return new PaymentResponse(PaymentResponse::SUCCESS,
                        "APPROVED:::\n".implode("\n",$lines)
                    );
                } else {
                    return new PaymentResponse(PaymentResponse::DECLINED,
                        "DECLINED:::\n".$mpgResponse->getMessage()."\n".implode("\n",$lines)
                    );
                }
            } else {
                return new PaymentResponse(PaymentResponse::ERROR_NO_RESPONSE,
                    "ERROR:::\nResponse was not received from gateway.\n"
                );
            }
        } catch (\Exception $e) {
            return new PaymentResponse(PaymentResponse::ERROR_EXCEPTION,
                "FATAL ERROR:::\n".$e->getMessage()
            );
        }
    }

    private function isComplete($value)
    {

        switch (strtolower($value)) {
            case 'true':
                return true;
                break;
            case 'false':
                return false;
                break;
        }
    }
}
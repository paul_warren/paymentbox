<?php

namespace Tomahawk\PaymentBox\PaymentMethods;

use Tomahawk\PaymentBox\PaymentMethod;
use Tomahawk\PaymentBox\PaymentRequest;
use Tomahawk\PaymentBox\PaymentResponse;

class TransactionSuccess extends PaymentMethod
{
    protected $action = self::PROCESS;

    public function attempt($total = 0.00, PaymentRequest $request)
    {
        sleep(2);

        return new PaymentResponse(PaymentResponse::SUCCESS,
            "APPROVED:::
========== TRANSACTION RECORD ==========

Paymentech Merchant Plug-in Test Store
44 King Street West
Toronto, ON M5H 1H1
Canada
chase.e-xact.com

TYPE:   Purchase

ACCT:   Visa  {$total} CAD

CARD NUMBER: ############1111
TRANS. REF : {$request->refNum}
CARD HOLDER:   TESTING
EXPIRY DATE: 01/15
DATE/TIME  :   06 Aug 13 03:43:08
BANK REF. #:   008 570643 E
AUTHOR. #  :   ET5817

     Approved - Thank You 000

SIGNATURE"
        );
    }
}
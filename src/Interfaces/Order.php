<?php

namespace Tomahawk\PaymentBox\Interfaces;

interface Order
{
    /**
     * @return mixed return the uniqueID generated for the order.
     */
    public function getUniqueID();

    /**
     * @return mixed return the security token generated for the order.
     */
    public function getToken();

    /**
     * @return double return the total value of the order to be charged.
     */
    public function getTotal();


}
<?php

namespace Tomahawk\PaymentBox\Interfaces;

use Tomahawk\PaymentBox\PaymentRequest;
use Tomahawk\PaymentBox\PaymentResponse;

interface PaymentTransaction
{
    // public function getOrderAttempt($orderAttempt);

    public function storeTransaction($orderAttempt, PaymentRequest $paymentRequest, PaymentResponse $paymentResponse);

    public function processSuccess($orderAttempt);

    public function processFailure($orderAttempt);
}
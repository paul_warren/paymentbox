<?php

namespace Tomahawk\PaymentBox;

class PaymentRequestParameter
{
    public $required = false;
    public $trim = false;
    public $length = 30;

    public function __construct($required = null, $trim = null, $length = null)
    {
        if ($required !== null) {
            $this->required = $required;
        }
        if ($length !== null) {
            $this->length = $length;
        }
        if ($trim !== null) {
            $this->trim = $trim;
        }
    }
}
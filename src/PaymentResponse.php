<?php

namespace Tomahawk\PaymentBox;

class PaymentResponse
{
    const SUCCESS = 1;
    const FAILED = -1;
    const ERROR_EXCEPTION = -2;
    const ERROR_NO_RESPONSE = -3;
    const DECLINED = -4;

    public $success = self::SUCCESS;
    public $log = '';

    public function __construct($success, $log)
    {
        $this->success = $success;
        $this->log     = $log;
    }
}
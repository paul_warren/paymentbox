<?php

namespace Tomahawk\PaymentBox;

class PaymentMethod
{
    const DEFERRED = 1;
    const REDIRECT = 2;
    const PROCESS = 3;

    protected $action = self::PROCESS;
    protected $redirect = null;

    protected $clientid;
    protected $password;
    protected $clientname;

    public function __get($name)
    {
        if ($name == 'action') {
            return $this->action;
        }
        if ($name == 'redirect') {
            return $this->redirect;
        }
    }

    public function maskCreditCard($value)
    {
        return '############' . substr(trim($value), -4);
    }
}
<?php

namespace Tomahawk\PaymentBox;

use Tomahawk\PaymentBox\Exceptions\InvalidException;
use Tomahawk\PaymentBox\Interfaces\Order;
use Tomahawk\PaymentBox\Interfaces\PaymentTransaction;

/**
 * Class PaymentBox
 * @package Tomahawk\PaymentBox
 */
class PaymentBox
{
    protected $order;
    protected $methods = [];
    protected $selectedMethod;
    protected $transactionRecorder;

    public function __construct($order = null, $transactionRecorder = null, $methods = [])
    {
        if ($order) {
            $this->setOrder($order);
        }
        if ($transactionRecorder) {
            $this->setTransactionRecorder($transactionRecorder);
        }
        if ($methods) {
            $this->setAvailablePaymentMethods($methods);
        }
    }

    public function setOrder($order)
    {
        if ( ! ($order instanceof Order)) {
            throw new InvalidException('The object supplied needs to be an instance of Order.');
        }

        $this->order = $order;
    }

    public function setTransactionRecorder($recorder)
    {
        if ( ! ($recorder instanceof PaymentTransaction)) {
            throw new InvalidException('The selected payment method is not available.');
        }

        $this->transactionRecorder = $recorder;
    }

    public function setAvailablePaymentMethods($methods)
    {
        foreach ($methods as $method) {
            if ( ! is_a($method, "Tomahawk\\PaymentBox\\PaymentMethod")) {
                throw new InvalidException('The supplied method is not a valid payment method.');
            }
        }
        $this->methods = array_merge($this->methods, $methods);
    }

    public function getAvailablePaymentMethods()
    {
        return $this->methods;
    }

    public function selectMethod($method)
    {
        if ( ! isset($this->methods[$method])) {
            throw new InvalidException('The selected payment method is not available.');
        }

        $this->selectedMethod = $this->methods[$method];
    }

    /**
     * @param $total float. The total value to charge.
     *
     * @throws InvalidException if no method was previously selected.
     */
    public function charge($total, PaymentRequest $request)
    {
        if ( ! is_a($this->selectedMethod, "Tomahawk\\PaymentBox\\PaymentMethod")) {
            throw new InvalidException('No payment method has been selected.');
        }
        if ( ! ($this->order instanceof Order)) {
            throw new InvalidException('The order has not been defined.');
        }

        // order must be set
        // desired payment method must be set.
        switch ($this->selectedMethod->action) {
            case PaymentMethod::DEFERRED:
                dd("NOT SETUP YET!");
                // store unprocessed payment, leave as is, and return.
                break;
            case PaymentMethod::REDIRECT:
                // redirect the user to the site.
                dd("NOT SETUP YET");
                // build the URL, the return back to send the client to that URL.
                // Included is a callback with the sessionID (we need the sessionID as that's how it will get the reservations needed to create the order)
                // When the user returns. they go to the callback url and the order is finalized there.
                // contact payment provider to store an attempt to be made, provide the callback url.
                // TODO A callback to process the payment after the fact is necessary.
                break;
            case PaymentMethod::PROCESS:
                $transactionResponse = $this->selectedMethod->attempt($total, $request);
                if ( ! ($transactionResponse instanceof PaymentResponse)) {
                    throw new InvalidException('Transaction did not return a proper response');
                }


                if ($this->transactionRecorder instanceof PaymentTransaction) {
                    $this->transactionRecorder->storeTransaction($this->order, $request, $transactionResponse);
                    if ($transactionResponse->success === PaymentResponse::SUCCESS) {
                        $this->transactionRecorder->processSuccess($this->order);
                    } else {
                        $this->transactionRecorder->processFailure($this->order);
                    }
                }
                break;
        }
    }

    public function callback()
    {
        dd("NOT SETUP YET.");
    }
}